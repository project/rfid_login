#!/usr/bin/env python
#
# Originally derived from the python drupal services module
# http://github.com/guaka/python_drupal_services
#

from ctypes import *
import sys

from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, OutputChangeEventArgs, TagEventArgs
from Phidgets.Devices.RFID import RFID

import xmlrpclib, time, random, string, hmac, hashlib, pprint

import webbrowser
 
class BasicServices(xmlrpclib.Server):
    """Drupal Services without keys or sessions, not very secure."""
    def __init__(self, url):
        xmlrpclib.Server.__init__(self, url)
        self.connection = self.system.connect()
        self.sessid = self.connection['sessid']
 
    def call(self, method_name, *args):
        # Maybe there's something more pythonic than eval?
        return self.__eval('self.' + method_name +
                           '(' +
                           ', '.join(map(repr,
                                         self._build_eval_list(method_name, args))) +
                           ')')
 
    def _build_eval_list(self, method_name, args):
        # method_name is used in ServicesSessidKey
        return args
 
    def __eval(self, to_eval):
        print to_eval
        try:
            return eval(to_eval)
        except xmlrpclib.Fault, err:
            print "Oh oh. An xmlrpc fault occurred."
            print "Fault code: %d" % err.faultCode
            print "Fault string: %s" % err.faultString

class ServicesSessid(BasicServices):
    """Drupal Services with sessid."""
    def __init__(self, url, username, password):
        BasicServices.__init__(self, url)
        self.session = self.user.login(self.sessid, username, password)
 
    def _build_eval_list(self, args):
        return ([self.sessid] +
                map(None, args)) # Python refuses to concatenate list and tuple
    
 
 
class ServicesSessidKey(ServicesSessid):
    """Drupal Services with sessid and keys."""
    def __init__(self, url, username, password, domain, key):
        BasicServices.__init__(self, url)
        self.domain = domain
        self.key = key
        self.session = self.call('user.login', username, password)
        self.sessid = self.session['sessid']
 
    def _build_eval_list(self, method_name, args):
        hash, timestamp, nonce = self._token(method_name)
        return ([hash,
                 self.domain, timestamp,
                 nonce, self.sessid] +
                map(None, args))
 
    def _token(self, api_function):
        timestamp = str(int(time.mktime(time.localtime())))
        nonce = "".join(random.sample(string.letters+string.digits, 10))
        return (hmac.new(self.key, "%s;%s;%s;%s" %
                         (timestamp, self.domain, nonce, api_function),
                         hashlib.sha256).hexdigest(),
                timestamp,
                nonce)

class ServicesKey(BasicServices):
    """Drupal Services with keys."""
    def __init__(self, url, domain, key):
        BasicServices.__init__(self, url)
        self.domain = domain
        self.key = key
 
    def _build_eval_list(self, method_name, args):
        hash, timestamp, nonce = self._token(method_name)
        return ([hash,
                 self.domain,
                 timestamp,
                 nonce] +
                map(None, args))
 
    def _token(self, api_function):
        timestamp = str(int(time.mktime(time.localtime())))
        nonce = "".join(random.sample(string.letters+string.digits, 10))
        return (hmac.new(self.key, "%s;%s;%s;%s" %
                         (timestamp, self.domain, nonce, api_function),
                         hashlib.sha256).hexdigest(),
                timestamp,
                nonce)
 
 
class DrupalServices:
    """Drupal services class.
    config is a nice way to deal with configuration files."""
    def __init__(self, config):
        self.config = config
        if (config.has_key('username') and config.has_key('key')):
            self.server = ServicesSessidKey(config['url'],
                                            config['username'], config['password'],
                                            config['domain'], config['key'])
        elif (config.has_key('username')):
            self.server = ServicesSessid(config['url'],
                                         config['username'], config['password'])
        elif (config.has_key('key')):
            self.server = ServicesKey(config['url'],
                                      config['domain'], config['key'])
        else:
            self.server = BasicServices(config['url'])
 
    def call(self, method_name, *args):
        # It would be neat to add a smart __getattr__ but that would
        # only go one level deep, e.g. server.node, not
        # server.node.save.
        return self.server.call(method_name, *args)
 
    def listMethods(self):
        return self.server.system.listMethods()
    
    def getInfo(self, method_name):
        print method_name
        print self.server.system.methodHelp(fName)
        print self.server.system.methodSignature(fName)

def rfidTagGained(e):
    global browser
    login_url = drupal.call('rfid_login.authenticate', e.tag)
    print login_url
    if login_url:
         webbrowser.open(login_url)

# Actual program starts here
from config import config
drupal = DrupalServices(config)

# Establish the connection to the RFID device
try:
    rfid = RFID()
except RuntimeError as e:
    print("Runtime Exception: %s" % e.details)
    print("Exiting")
    exit(1)

# This program is event driven, so assign the event
try:
    rfid.setOnTagHandler(rfidTagGained)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    try:
        rfid.closePhidget()
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))
        print("Exiting")
        exit(1)
    print("Exiting")
    exit(1)

# Let the user know what we're doing
print("Opening phidget object....")

# Actually open the phidget connection
try:
    rfid.openPhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Waiting for attach....")

# Set a timeout, if we dont connect within it.
try:
    rfid.waitForAttach(10000)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    try:
        rfid.closePhidget()
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))
        print("Exiting....")
        exit(1)
    print("Exiting....")
    exit(1)

# Turn it on
print("Turning on the RFID antenna")
rfid.setAntennaOn(True)

# Run until the user pushes enter
print("Press Enter to quit")

chr = sys.stdin.read(1)

print("Closing")

# Cleanup nicely
try:
    rfid.closePhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting")
    exit(1)
    

print("Done")
exit(0)
