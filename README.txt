Want to amaze your friends?
Sick of logging into drupal the "old fashioned" way?
Think that RFID access would be cool?

Then this module is for you!

To login to drupal, simply swipe the RFID tag and a browser will be started (if required) and logged in instantly as your user.

This module was built to enable instant login for Point of Sale with the ERP module by StratosERP.

The rfid_login module utilises services and a user login for security, whilst still enabling amazing convenience.

This module uses example code from:
http://www.phidgets.com/programming_resources.php
http://github.com/guaka/python_drupal_services

Thanks to both these projects for making the source available.
