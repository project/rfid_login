"""
General settings for connecting to Drupal. The key can be set by
visiting /admin/build/services/keys on your Drupal installation).
 
"""
 
config_local = {
  'url': 'http://webaddress/xmlrpc',
  'key': 'key from services',
  'domain': 'domain from services',
  'username': 'valid drupal user',
  'password': 'and their password',
}
 
config = config_local
