<?php

/**
 * @file
 * Functions for rfid_login module services calls
 */


/**
 * Main authenticate function that looks up user-> tag association
 *
 */
function rfid_login_authenticate($tag) {
  // We've already been through the services auth at this stage, so we can assume security
  $uid = db_result(db_query("SELECT uid FROM {rfid_login} WHERE tag = '%s'", $tag));
 
  // Add in the tag so that it can be selected by the admin
  if (!$uid) {
  	db_query("INSERT INTO {rfid_login} (uid, tag) VALUES (-1, '%s')", $tag);
    watchdog('rfid_login', 'Added new RFID tag: '. $tag);
    return "";
  }

  // Check if this is an unassigned tag
  if ($uid == -1) {
    watchdog('rfid_login', 'Unassigned tag used: '. $tag);
    return "";
  }
  else { // Its not, load the associated used
  	$account = user_load(array('uid' => $uid));
  }

  // Check we were able to load the user (they might have been deleted or something
  if ($account) {
    $timestamp = time();
    return url("rfidlogin/". $account->uid ."/". $timestamp ."/". user_pass_rehash($account->pass, $timestamp, $account->login), array('absolute' => TRUE)); 
  }
  else {
    watchdog('rfid_login', 'Invalid user assigned to tag: '. $tag);
    return "";
  }
}
